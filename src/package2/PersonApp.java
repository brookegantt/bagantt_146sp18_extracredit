/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package package2;

import package1.*;
import package3.*;

/**
 *
 * @author brookegantt
 */
public class PersonApp {
    public static void main(String[] args )
    {
        Person person = new Person();
        
        person.setName("Brian");
        person.setAge(42);
        
        Professor person2 = new Professor();
        
        person2.setName("Dr. Canada");
        person2.setAge(75);
        person2.setRank("Associate");
        
        person2.teach();
    }// end method main
}// end class personapp
