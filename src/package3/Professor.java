/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package package3;
import package1.*;

/**
 *
 * @author brookegantt
 */
public class Professor extends Person {
    private String rank;

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }
    
    
    
    public void teach()
    {
        System.out.println(getName() + " is teaching you!"); 
    } // end method teach
} // end class professor
